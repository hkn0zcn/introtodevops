# Objective
Make sure you know a few basic bash commands that will make your life easier during this class.

 - cd - change directory
 - ls - list contents of directory
 - pwd - print working directory
 - mkdir - make a new directory
 - cat - list out contents of a file

# Bash Warmup

If you're new to Bash, here are a few things that will help:

The tildae
- In Bash, `~` is a shortcut meaning "this user's home directory"
- So when the user `fbaggins` is logged in to a system, `mkdir ~/sfs` is a shortcut for `mkdir /home/fbaggins/sfs`.  (Probably.  Frodo's home directory could be somewhere else.)

Paths: `/my_folder/my_file` vs. `my_folder/my_file`
- In a Bash shell, the `/` directory is the root of the filesystem.  Much like `C:\` on Windows.
- `/my_folder/my_file` is an absolute path to `my_file`
- `my_folder/myfile` is a relative path, which means...
  - If you're in `/tmp`:
    - `my_folder/my_file` is actually `/tmp/my_folder/my_file`
  - If you're in `/home/fbaggins`:
    - `my_folder/my_file` is actually `/home/fbaggins/my_folder/my_file`
  - etc.

![image](linux-dir.png?)

Tab completion
- Instead of typing out a directory or filename, type a few letters and press Tab.
- Press tab multiple times and see what happens.

Command history
- To re-run a previous command, press CTRL+R, then type part of the command you want to re-run
- A matching command should appear
- Press CTRL+R again to cycle through earlier matching commands

Variables
- The `$` indicates the start of a variable name
- `NAME=Frodo` sets `$NAME` to `Frodo`

Command substitution
- The `$()` syntax tells the shell to substitute the output of the command in place of the entire `$(...)` string
- `echo which date` will output `which date`
- `echo $(which date)` causes `which date` to be executed first, which results in `echo /bin/date`
- `echo $($(which date))` causes `which date` to be executed first, which results in the date being printed

  ```bash
  # date
  Wed Nov 30 14:34:08 MST 2016
  # echo which date
  which date
  # echo $(which date)
  /bin/date
  # echo $($(which date))
  Wed Nov 30 14:34:08 MST 2016
  ```

Logout
- Pressing CTRL+D will log you out of a shell

---

|Next: [GitLab](/labs/01_gitlab)|
|:---:|
