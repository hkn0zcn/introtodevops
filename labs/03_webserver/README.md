# A Really Basic WebServer (in Docker)

*By the end of this lab, you will be able to:*

 * build and run a local webserver container
 * select container images purposefully for your docker builds



For this lab, use the repository you created in the [GitLab lab](labs/01_gitlab).  
If you followed the instructions verbatim, it should be `~/sfs/mywebserver`

```bash
cd ~/sfs/mywebserver
```

---

### Make a Webpage

```bash
mkdir html
echo This is my first web page > html/index.html
xdg-open html/index.html
# if `xdg-open` doesn't work, try `open`
```

---

### Make a Webserver with Docker

Save this as a `Dockerfile` file:

```Dockerfile
FROM nginx

COPY ./html /usr/share/nginx/html/
```

And build your Docker image:

```bash
docker build -t webserver .
```

![image](make_webserver.png?)


---



### Run and Test Your Webserver

Run your webserver with the `-p 80:80` option to expose the ports, and (optionally) give it a name with the `--name` flag:

```bash
docker run --detach -p 80:80 --name mywebserver webserver
```

Now run `docker ps` to see information about your running container:

```
CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS                   NAMES
c937baecda34        webserver           "nginx -g 'daemon of…"   3 seconds ago       Up 1 seconds        0.0.0.0:80->80/tcp   mywebserver
```

Finally, test your webserver to see if it's running:

```bash
xdg-open http://localhost

```

You should also be able to test http://localhost in your web browser.

---

### Docker Hub - [hub.docker.com](https://hub.docker.com)
  - [Explore](https://hub.docker.com/search?&q=)

---

| Previous: [Docker](/labs/02_docker) | Next: [Continuous Integration](/labs/04_continuous_integration) |
|---:|:---|
