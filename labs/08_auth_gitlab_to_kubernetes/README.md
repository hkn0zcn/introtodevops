# Authorizing Deploys from GitLab to Kubernetes

*By the end of this lab, you will:*

- Configure your pipeline to deploy your custom webserver on your Kubernetes cluster
- Use GitLab CI/CD Variables
- Use a GCP IAM service account to connect to your cluster from GitLab

---

You have provided your Kubernetes cluster with credentials to the private container registry in your GitLab project.  But GitLab currently has no way to authenticate to your Kubernetes cluster to update the manifest.  We're going to create a service account to solve this problem.

### Kubernetes Service Account

1. Store your project name as a variable to easy use throughout this lab

  ```bash
  export GCP_PROJECT=$(gcloud config get-value project)
  echo ${GCP_PROJECT}  # >>> If this doesn't show your project name, panic at the teacher
  ```

2. Create the service account in your GCP project

  ```bash
  gcloud iam service-accounts create gitlab-deploy \
    --display-name "gitlab-deploy" \
    --project ${GCP_PROJECT}
  ```

3. Grant the `container.admin` role to the `gitlab-deploy` service account

  ```bash
  gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
    --member serviceAccount:gitlab-deploy@${GCP_PROJECT}.iam.gserviceaccount.com \
    --role roles/container.admin
  ```

4. Create a key file ... this should make sense in a minute.

  ```bash
  mkdir -p ~/sfs/work-dir/gcp-config

  gcloud iam service-accounts keys create \
    ~/sfs/work-dir/gcp-config/gitlab-deploy-${GCP_PROJECT}.json \
    --iam-account gitlab-deploy@${GCP_PROJECT}.iam.gserviceaccount.com
  ```

### Configuring the Service Account from GitLab

1. Examine your key file

  ```bash
  cat ~/sfs/work-dir/gcp-config/gitlab-deploy-${GCP_PROJECT}.json
  ```

  **Do not use this one; use your own.** But it should look something like this:

  ```json
  {
    "type": "service_account",
    "project_id": "i2do-with-k8s",
    "private_key_id": "40502a36f9f9f31765d6e5e7e5462ce5ac0d23b5",
    "private_key": "-----BEGIN PRIVATE KEY-----\nBigLongSSHKey\n-----END PRIVATE KEY-----\n",
    "client_email": "gitlab-deploy@i2do-with-k8s.iam.gserviceaccount.com",
    "client_id": "104422447094047755381",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/gitlab-deploy%40i2do-with-k8s.iam.gserviceaccount.com"
  }
  ```

2. Navigate to `MyWebServer --> Settings --> CI/CD --> Variables`

3. Create a new variable called `GCP_AUTH_KEYFILE`, and paste the _entire contents_ of the file as the value for the variable.

  ![image](keyfile_variable.png)

4. Save the variable.

### Using the Service Account

1. Add a `deploy_webserver` job to your pipeline file (`~/sfs/mywebserver/.gitlab-ci.yml`):

  **You can copy this one as a starting point, but make sure to update your `variables` section!**

  ```yaml
  build_and_test_webserver:
    stage: build
    image: docker:stable
    services:
      - docker:dind
    script:
      - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
      - docker run -d -p8080:80 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
      - docker logs mywebserver
      - apk add curl
      - curl docker:8080
      - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
      - docker push ${CI_REGISTRY_IMAGE}
      - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}

  deploy_webserver:
    stage: deploy
    image: google/cloud-sdk:latest
    variables:
      CLUSTER: standard-cluster-1
      REGION: us-central1-a
      PROJECT: i2do-with-k8s
    script:
      # Connect to the Kubernetes cluster
      - echo ${GCP_AUTH_KEYFILE} > /root/.config/keyfile.json
      # This is the service-account version of the `gcloud auth login` we ran manually/locally
      - gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
      # These are the same as our local/manual commands
      - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${PROJECT}
      - kubectl apply -f webserver-manual.yaml
  ```

3. Commit your changes

  ```bash
  cd ~/sfs/mywebserver
  git add webserver-manual.yaml
  git commit -m 'add the k8s deploy manifest'
  git add .gitlab-ci.yml
  git commit -m 'add a k8s deploy job to the pipeline'
  git push
  ```

4. Check your pipeline status.  

  - Note that your pipeline now has two jobs: `build_and_test_webserver` as well as `deploy_webserver`

  - Look for the following in the output of the `deploy_webserver` job:

    **Do not copy/paste this in your terminal.  Just look for it in the CI job output.**

    ```
    $ gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
    Activated service account credentials for: [gitlab-deploy@i2do-with-k8s.iam.gserviceaccount.com]
    $ gcloud container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${PROJECT}
    Fetching cluster endpoint and auth data.
    kubeconfig entry generated for standard-cluster-1.
    $ kubectl apply -f webserver-manual.yaml
    deployment.apps/mywebserver-dev unchanged
    service/mywebserver-dev unchanged
    ingress.extensions/mywebserver-dev unchanged
    Job succeeded
    ```

---

| Previous: [K8s to GitLab Auth](/labs/07_auth_kubernetes_to_gitlab) | Next: [Automated Deployment](/labs/09_automated_deployment) |
|---:|:---|
