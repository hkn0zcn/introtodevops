# Accessing a Private Registry from Kubernetes

*By the end of this lab, you will:*

- Launch your custom webserver on your Kubernetes cluster
- Install Docker credentials in your K8s cluster

---

Our webserver is in a private repository.  We need to [give Kubernetes access](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/).  We will do so with a [Deploy Token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) from our GitLab project.


### A Deploy Token for Kubernetes

1. Navigate to `MyWebServer --> Settings --> Repository --> Deploy Tokens`

2. Create a token.  You can use anything you want for the name, expiration, or username.  Make sure the `read_registry` box is checked.

  ![image](deploy_tokens.png)

3. After creating the token, GitLab will only show it to you once.  Do not navigate away from this page yet.

  ![image](token_display.png)

4. If you navigated away from the page, you can delete and re-create the token.

5. We're going to use a special Kubernetes command to put Docker credentials on our cluster.  _Substitute your username and password in this command!_  (Email address doesn't matter.)

  ```bash
  kubectl create secret docker-registry regcred \
    --docker-server=registry.gitlab.com \
    --docker-username=gitlab+i2do-k8s-cluster \
    --docker-password=c-izCHph3fxLLb9RB-5j \
    --docker-email=dev@null.com
  ```

6. Examine the secret you just created:

  ```bash
  kubectl get secret regcred --output=yaml
  ```

  To see **A)** more detail and **B)** that secrets aren't very secret:

  ```bash
  kubectl get secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode | jq
  ```

  You should see something like this:

  ```json
  {
    "auths": {
      "registry.gitlab.com": {
        "username": "gitlab+i2do-k8s-cluster",
        "password": "c-izCHph3fxLLb9RB-5j",
        "email": "dev@null.com",
        "auth": "Z2l0bGFiK2RlcGxveS10b2tlbi0xMjY1NTk6dW5HN0xfcFN6bnVqb1VWemp2WVM="
      }
    }
  }
  ```


### Testing the Credentials

1. Update `webserver-manual.yaml`

  - In the `Deployment`, update `.spec.template.spec.containers[0].image` from `nginx` to the container you've built from your CI pipeline.

    **Make sure to substitute your own image path!**  Your deploy key won't work on `aayore`'s registry.

    `registry.gitlab.com/<your_gitlab_user_name>/mywebserver:latest`

  - Also in the `Deployment`, in its `.spec.template.spec` hash, add `imagePullSecrets` as an array containing the credentials secret you just created.

  The `.spec.template.spec` block should look (kinda) like this:

  ```yaml
  ...
  spec:
    imagePullSecrets:
      - name: regcred
    containers:
      - name: webserver
        image: registry.gitlab.com/aayore/mywebserver:latest
        imagePullPolicy: IfNotPresent
        ports:
          - name: default
            containerPort: 80
            protocol: TCP
  ...
  ```

2. Apply the updates with

  ```bash
  kubectl apply -f webserver-manual.yaml
  ```

3. Watch the new pod take the place of the old pod

  Working with a partner, select one of these commands to run and have your partner run the other:

  ```bash
  # Option A
  kubectl get pod --watch

  # Option B
  watch kubectl get pod
  ```

  Which is easier to read?

  Use `Ctrl+C` to terminate that command once the old pod is gone, then...

4. Test the web page

  ```bash
  curl $(kubectl get ing mywebserver-dev -o jsonpath='{.spec.rules[0].host}')
  ## or, for example... ##
  curl dev.teacher.IntroToDevOps.com
  ```

  You should see the web page you built into the container earlier - not the nginx welcome page.

---

| Previous: [Manual Deployment](/labs/06_manual_deployment) | Next: [GitLab to K8s Auth](/labs/08_auth_gitlab_to_kubernetes) |
|---:|:---|
