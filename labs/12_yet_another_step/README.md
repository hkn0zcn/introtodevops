# Tests++ --> Continuous Deployment

This iterates on our pipeline again.  We're adding an additional stage to test our deploy in the `dev` environment.  If this is successful, we automatically deploy to `prod`.

```yaml
stages:
  - build
  - deploy_dev
  - test_k8s
  - deploy_prod

build_and_test_webserver:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  script:
    - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
    - docker run -d -p8080:80 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
    - docker logs mywebserver
    - apk add curl
    - curl docker:8080
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
    - docker push ${CI_REGISTRY_IMAGE}
    - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}

.deploy:
  image: google/cloud-sdk:latest
  before_script:
    # Connect to the Kubernetes cluster
    - echo ${GCP_AUTH_KEYFILE} > /root/.config/keyfile.json
    - gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
    - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${PROJECT}
  script:
    # Install Helm for template parsing
    - curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
    - >
      helm template \
        --set image=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
        --set host=${INGRESS_HOST} \
        --set environment=${K8S_ENVIRONMENT} \
        deploy > manifest.yaml
    - kubectl apply -f manifest.yaml

deploy_dev:
  extends: .deploy
  stage: deploy_dev
  variables:
    CLUSTER: standard-cluster-1
    REGION: us-central1-a
    PROJECT: i2do-with-k8s
    INGRESS_HOST: dev.aaron.introtodevops.com
    K8S_ENVIRONMENT: dev
  environment: dev

test_dev_k8s:
  stage: test_k8s
  image: curlimages/curl:latest
  variables:
    INGRESS_HOST: dev.teacher.introtodevops.com
  script:
    - |
      for COUNTER in $(seq 1 10)
      do sleep 3
      if curl ${INGRESS_HOST}; then exit 0; fi
      done
      exit 1

deploy_prod:
  extends: .deploy
  stage: deploy_prod
  variables:
    CLUSTER: standard-cluster-1
    REGION: us-central1-a
    PROJECT: i2do-with-k8s
    INGRESS_HOST: www.aaron.introtodevops.com
    K8S_ENVIRONMENT: prod
  environment: prod
```

---

### More Abstraction

Maintaining these variables in multiple places in a file may not be ideal for your environment.  Especially when some of the variables (e.g. `CLUSTER`, `REGION`, and `PROJECT`) may be widely-used across projects.

- If you have a **group** configured in GitLab (e.g. https://gitlab.com/sofreeus), you might set the following group-wide:

  ```yaml
  DEV_CLUSTER: dev-cluster-1
  DEV_REGION: us-central1
  DEV_PROJECT: company-dev
  PROD_CLUSTER: prod-cluster-1
  PROD_REGION: us-central1
  PROD_PROJECT: company-prod
  ```

- Within your **project**, you could set these:

  ```yaml
  DEV_INGRESS_HOST: dev.teacher.introtodevops.com
  DEV_K8S_ENVIRONMENT: dev
  PROD_INGRESS_HOST: www.teacher.introtodevops.com
  PROD_K8S_ENVIRONMENT: prod
  ```

- Now you can configure your deploy jobs to look like this:

  ```yaml
  deploy_dev:
    extends: .deploy
    stage: deploy_dev
    variables:
      CLUSTER: ${DEV_CLUSTER}
      REGION: ${DEV_REGION}
      PROJECT: ${DEV_PROJECT}
      INGRESS_HOST: ${DEV_INGRESS_HOST}
      K8S_ENVIRONMENT: ${DEV_K8S_ENVIRONMENT}
    environment: dev

  deploy_prod:
    extends: .deploy
    stage: deploy_prod
    variables:
      CLUSTER: ${PROD_CLUSTER}
      REGION: ${PROD_REGION}
      PROJECT: ${PROD_PROJECT}
      INGRESS_HOST: ${PROD_INGRESS_HOST}
      K8S_ENVIRONMENT: ${PROD_K8S_ENVIRONMENT}
    environment: prod
  ```

This makes your `.gitlab-ci.yml` - maybe more importantly the individual jobs - more portable and reusable.  This means you could start playing with the [include](https://docs.gitlab.com/ee/ci/yaml/#include) directive...

---

| Previous: [One More Step](/labs/11_one_more_step) |  
|:---:|
