# Environments

The updated `.gitlab-ci.yml` below...

1. Uses a template job to create multi-stage deploys
  - Note the addition of custom stages
  - `.deploy` starts with `.` which means it won't run
  - `deploy_dev` and `deploy_prod` have been defined to use the `.deploy` job (via `extends`)
  - `deploy_dev` and `deploy_prod` have specific variables that guide the deployment

2. Adds the `environment` key to the deploy jobs
  - This tells GitLab to populate the `MyWebServer --> Operations --> Environments` page
  - Provides version history when you click in to an environment
  - Enables one-click rollbacks
  - You can optionally edit an environment to add the URL

### Try it Out

1. Update your `.gitlab-ci.yml`

  ```yaml
  stages:
    - build
    - deploy_dev
    - deploy_prod

  build_and_test_webserver:
    stage: build
    image: docker:stable
    services:
      - docker:dind
    script:
      - docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} -t ${CI_REGISTRY_IMAGE}:latest .
      - docker run -d -p8080:80 --name mywebserver ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
      - docker logs mywebserver
      - apk add curl
      - curl docker:8080
      - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
      - docker push ${CI_REGISTRY_IMAGE}
      - echo ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}

  .deploy:
    image: google/cloud-sdk:latest
    before_script:
      # Connect to the Kubernetes cluster
      - echo ${GCP_AUTH_KEYFILE} > /root/.config/keyfile.json
      - gcloud auth activate-service-account --key-file=/root/.config/keyfile.json
      - gcloud container clusters get-credentials ${CLUSTER} --region ${REGION} --project ${PROJECT}
    script:
      # Install Helm for template parsing
      - curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | bash
      - >
        helm template \
          --set image=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA} \
          --set host=${INGRESS_HOST} \
          --set environment=${K8S_ENVIRONMENT} \
          deploy > manifest.yaml
      - kubectl apply -f manifest.yaml

  deploy_dev:
    extends: .deploy
    stage: deploy_dev
    variables:
      CLUSTER: standard-cluster-1
      REGION: us-central1-a
      PROJECT: i2do-with-k8s
      INGRESS_HOST: dev.teacher.introtodevops.com
      K8S_ENVIRONMENT: dev
    environment: dev

  deploy_prod:
    extends: .deploy
    stage: deploy_prod
    when: manual
    variables:
      CLUSTER: standard-cluster-1
      REGION: us-central1-a
      PROJECT: i2do-with-k8s
      INGRESS_HOST: www.teacher.introtodevops.com
      K8S_ENVIRONMENT: prod
    environment: prod
  ```

2. Commit and push your changes:

  ```bash
  git add -A
  git commit -m 'major deploy updates'
  git push
  ```

3. Examine your pipeline

  - Note the additional job/stage
  - Regarding `deploy_prod`...
    - Note that the job will not run automatically
    - You cannot run the job before `deploy_dev` finishes
  - Visit the `MyWebServer --> Operations --> Environments` page in your repo

4. Examine your cluster

  ```bash
  kubectl get deploy,svc,ing
  ```

5. Examine your environments

  _Remember, these are examples; check your own URL's, too!_

  ```bash
  curl dev.teacher.introtodevops.com
  curl www.teacher.introtodevops.com
  ```

---

| Previous: [Everything!](/labs/10_everything) | Next: [Yet Another Step](/labs/12_yet_another_step) |
|---:|:---|
