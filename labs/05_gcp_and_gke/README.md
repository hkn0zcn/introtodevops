# Build a GKE cluster

Kubernetes the easy way: using GCP

*By the end of this lab, you will:*

- Create a GCP project from the GUI
- Create a GCP project from the CLI
- Build a Kubernetes cluster from the GUI
- Build a Kubernetes cluster from the CLI
- Run some basic `kubectl` commands

---

Recommended working directory: `~/sfs/work-dir/cloud-infrastructure`

---

### Verify GCP Access & Create a Project

Make sure you can sign in to the [GCP Console](https://console.cloud.google.com/)

1. Click the project selector menu
  ![project selector menu](project_selector_dropdown.png)
2. Click the `New Project` button in the top right corner of the modal that appears
3. Give your project a unique name
4. Make sure the location and organization are correct (instructor can verify)
5. Click the `Create` button

---

### Trigger API Enablement

Google Cloud Platform has an interesting quirk: everything is off by default.  The easiest way to enable API's/features is just to navigate to the appropriate page in the console.  This can take a few minutes, so we're going to do it now.

1. Go to the [GCP Console](https://console.cloud.google.com/)
2. Use the project selector menu (see picture above) to select the correct project for this class
  - If you don't see your project, click the `All` tab, or...
  - Use the search functionality
  - If you still can't find it, call the instructor over  :p
3. Pin `Kubernetes Engine` for easy access
  1. Click the hamburger menu at the top left corner of the console
  2. Find `Kubernetes Engine`
  3. Click the pin
4. Click `Kubernetes Engine` in the menu

If the notifications don't pop up automatically, you can show them by clicking the bell icon at the top right of the console.  You should see the following notifications:
- `Create Project ...`
- `Initializing Kubernetes Engine for project ...`
- `Initializing Compute Engine for project ...`

There may be multiples - that's okay.  You can move on to the next section before these finish.

---

### Configure the CLI

There are two different sets of instructions here.  If you have never used `gcloud` before (`which gcloud` should return `gcloud not found`), you can skip the first set of instructions.

#### Instructions for users who already have `gcloud` set up

You will be adding and activating a new configuration.

1. Run `gcloud config configurations list` to see your existing configurations
2. Create a backup of your current configuration:
  - `cp -R ~/.config/gcloud/configurations ~/gcloud-config-backup-$(date +%Y-%m-%d)`
3. Create a new configuration for this class:
  1. `gcloud init`
  2. Choose the option to `Create a new configuration`
  3. Enter a configuration name: `sfs`
  4. Select the account you used above
  5. Perform the browser authentication/authorization
  6. Select the project you created above
  7. If prompted, configure a default region and zone
    - Pick any of the `us-central1` options
4. Verify you're using the freshly-minted configuration:
  - `gcloud config list` should show `Your active configuration is: [sfs]`
5. At the end of class, you can switch back to your original configuration using the `gcloud config configurations activate <configuration_name>` [command](https://cloud.google.com/sdk/gcloud/reference/config/configurations/activate)
6. Skip the following instructions for new `gcloud` users

#### Instructions for new `gcloud` users

1. Follow the Google Docs to [install the Google Cloud SDK](https://cloud.google.com/sdk/docs/) (`gcloud`) on your machine.
2. Run `gcloud init` from your command line ([command documentation](https://cloud.google.com/sdk/docs/initializing))
  1. When prompted, select the option to log in
  2. A new browser window/tab should open; authorize the same account you used for the Console login
  3. When you see `You are now authenticated with the Google Cloud SDK!`, return to your command line
  4. When prompted to select a Cloud Platform project, choose the one you created above
  5. If prompted, configure a default region and zone
    - Pick any of the `us-central1` options
3. Verify you're using the freshly-minted configuration:
  - `gcloud config list` should show `Your active configuration is: [default]`

---

### Launch a Kubernetes Cluster from the GUI

1. Log in to the [console](https://console.cloud.google.com)
2. Select your project from the drop-down at the top
3. Navigate to `Kubernetes Engine` console
4. Click the `Create cluster` button
5. From the `Cluster templates` menu on the left, select `Standard cluster`
6. Configure the cluster options per the table below; _do not click the `Create` button yet!_  If an option is not included in the table, use the default.
7. Click the `More options` button
8. Check the `Enable preemptible nodes` checkbox
9. Click `Save`
10. Find the `Equivalent REST or command line` link at the bottom of the screen.
11. Click the `command line` link.
12. Save this gnarly command in a new file at `~/sfs/work-dir/new-cluster.sh`
13. In `~/sfs/work-dir/new-cluster.sh`, change `<your-name>-gui` to `<your-name>-cli`
14. In the console, close the `command line` modal if it's still open
15. Click the `Create` button to build your cluster

| Option                     | Value             |
| -------------------------- | :---------------: |
| Name                       | `<your-name>-gui` |
| Machine type               | g1-small          |

Example command (**do not copy/paste this one**), cleaned up and saved at `~/sfs/work-dir/new-cluster.sh`:
```bash
gcloud beta container --project "i2do-with-k8s" \
  clusters create "standard-cluster-2" \
  --zone "us-central1-a" \
  --no-enable-basic-auth \
  --cluster-version "1.13.11-gke.14" \
  --machine-type "g1-small" \
  --image-type "COS" \
  --disk-type "pd-standard" \
  --disk-size "100" \
  --metadata disable-legacy-endpoints=true \
  --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" \
  --preemptible \
  --num-nodes "3" \
  --enable-cloud-logging \
  --enable-cloud-monitoring \
  --enable-ip-alias \
  --network "projects/i2do-with-k8s/global/networks/default" \
  --subnetwork "projects/i2do-with-k8s/regions/us-central1/subnetworks/default" \
  --default-max-pods-per-node "110" \
  --addons HorizontalPodAutoscaling,HttpLoadBalancing \
  --enable-autoupgrade \
  --enable-autorepair
```

---

### Launch a Kubernetes Cluster From the CLI

1. Edit `~/sfs/work-dir/new-cluster.sh`
  - Add `#!/bin/bash` to the top of the file
2. `chmod +x ~/sfs/work-dir/new-cluster.sh`
3. Build another cluster!
  - `~/sfs/work-dir/new-cluster.sh`

That was easy, right?  You now have a script that you can update or modify to create new clusters as necessary!

---

## Delete a Cluster

- Now that you've built a cluster using both the GUI and the CLI, choose a favorite and delete the other.  You only need one for the rest of the class.

---

## `kubectl` Installation

Check to see if you already have `kubectl` installed:

```bash
$ which kubectl
/usr/local/bin/kubectl
$ kubectl version
Client Version: version.Info{Major:"1", Minor:"15", GitVersion:"v1.15.2", GitCommit:"f6278300bebbb750328ac16ee6dd3aa7d3549568", GitTreeState:"clean", BuildDate:"2019-08-05T16:54:35Z", GoVersion:"go1.12.7", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"13+", GitVersion:"v1.13.11-gke.14", GitCommit:"56d89863d1033f9668ddd6e1c1aea81cd846ef88", GitTreeState:"clean", BuildDate:"2019-11-07T19:12:22Z", GoVersion:"go1.12.11b4", Compiler:"gc", Platform:"linux/amd64"}
```

##### Preferred installation method for this class
- `gcloud components install kubectl`
- This makes updates easy: `gcloud components update`

##### [Secondary recommendations](https://kubernetes.io/docs/tasks/tools/install-kubectl)
- [Kubernetes](https://kubernetes.io/) maintains a list of [installation instructions.](https://kubernetes.io/docs/tasks/tools/install-kubectl)
- Using [Homebrew](https://brew.sh/) for Mac is nice.  Facilitates updates.
- `kubectl` should be in the main repos for Linux distros.

## `kubectl` Auth

```bash
# Configure
# (You can easily copy this command from the GCP/GKE UI with the `Connect` button)
gcloud container clusters get-credentials ${CLUSTER_NAME} --region ${CLUSTER_REGION}

# Verify
kubectl get nodes # >>> Make sure `${CLUSTER_NAME}` is in the output
```

<!--
**Deprecated:**
>
> On GKE clusters v1.11.x and earlier, you have to enable RBAC by creating a binding to give your account `cluster-admin` permissions.
>
> ```bash
> # Grant your account admin access:
> kubectl create clusterrolebinding cluster-admin-binding-$(date "+%Y-%m-%d") \
>   --clusterrole cluster-admin \
>   --user $(gcloud config get-value account)
> ```

NOTE: NGINX DOCS STILL SAY THIS IS REQUIRED!!!  THIS NEEDS TESTING!!!

More information regarding the [RBAC implementation w/ GKE](https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control).
-->

---

## Kubernetes Nginx Ingress

```bash
# Label our nodes (because ingress-nginx expects it; this requirement goes away with Kubernetes 1.14+)
kubectl label nodes --all kubernetes.io/os=linux

# Scaffolding for the ingress:
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/mandatory.yaml

# Ingress controller service:
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/cloud-generic.yaml
```

These commands will create a number of new objects on your Kubernetes cluster.  A brief summary will be shown in the terminal.

Next, we want to put your cluster in DNS.  Run this command and watch the output:

```bash
watch kubectl -n ingress-nginx get service
```

When `EXTERNAL-IP` changes from `<pending>` to an IP, type Ctrl+C to exit the `watch` command.  Then work with your teacher to complete the configuration in DNS.

Ultimately, we're going to create a wildcard record for your cluster's external IP address.  This means that something like `*.teacher.IntroToDevOps.com` will route all traffic to the `teacher`'s Kubernetes cluster.

<!--
Deprecated as of v1.11.x:
```
# Remove the admin access for your account:
kubectl delete clusterrolebinding cluster-admin-binding-$(date "+%Y-%m-%d")
```
-->

---

| Previous: [Continuous Integration](/labs/04_continuous_integration) | Next: [Manual Deployment](/labs/06_manual_deployment) |
|---:|:---|
