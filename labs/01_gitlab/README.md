# GitLab

*By the end of this lab, you will:*
1. Setup SSH keys and use them to configure gitlab
1. Create and clone a new gitlab repo
1. Commit and push to your new gitlab repo
1. Clone an existing gitlab repo
1. Create a new git repo

---

### Set Up a WorkDir

- Create a directory to house your work for today's class

  ```bash
  mkdir -p ~/sfs
  cd ~/sfs
  ```

---

### Get Started (Authentication)

- Set up [SSH keys](https://docs.gitlab.com/ce/gitlab-basics/create-your-ssh-keys.html)
  - Check to see if you have a public/private key pair in `~/.ssh/`
    - If so, you should see `id_rsa` and `id_rsa.pub` in that directory
    - If not, run `ssh-keygen` to generate a key pair

    ```
    osadmin@SFS-Eoan:~/sfs$ ls -l ~/.ssh
    total 0
    osadmin@SFS-Eoan:~/sfs$ ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/osadmin/.ssh/id_rsa):
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/osadmin/.ssh/id_rsa.
    Your public key has been saved in /home/osadmin/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:FFIqgARUVKCHZsPFUKiu5deedfWgT5iq1rxzLEx8xsE osadmin@SFS-Eoan
    The key's randomart image is:
    +---[RSA 3072]----+
    |      blah       |
    +----[SHA256]-----+
    osadmin@SFS-Eoan:~/sfs$ ls -l ~/.ssh
    total 8
    -rw------- 1 osadmin osadmin 2602 Jan  9 11:54 id_rsa
    -rw-r--r-- 1 osadmin osadmin  570 Jan  9 11:54 id_rsa.pub
    ```

  - Log in to [GitLab](https://www.gitlab.com), navigate to the [SSH Keys tab of your profile settings](https://gitlab.com/profile/keys), and add the contents of your `id_rsa.pub` file as a new key.

  ```
  osadmin@SFS-Eoan:~/sfs$ cat ~/.ssh/id_rsa.pub
  ssh-rsa aBunchOfStuffHere!........................... osadmin@SFS-Eoan
  ```

- Add a [personal access token](https://gitlab.com/profile/personal_access_tokens) with API Scope to your GitLab account settings.

![image](PAT1.png?)

When you create the token, the page will reload and you will see this _once_.  If you don't copy it now, you'll have to delete and recreate it.  

![image](PAT2.png?)

Add **your** token to your `~/.bash_profile` (you may have to create this file):
```bash
# Don't add this.  This is Teacher's.  Add YOUR token.
export GITLAB_API_TOKEN="vzN7Y2ynrydvo4WFNycc"
```

Then load your update and make sure you can see the token:

```bash
source ~/.bash_profile
echo $GITLAB_API_TOKEN
```

You should see this:

```
osadmin@SFS-Eoan:~/sfs$ source ~/.bash_profile
osadmin@SFS-Eoan:~/sfs$ echo $GITLAB_API_TOKEN
vzN7Y2ynrydvo4WFNycc  # You won't see this; you'll see YOUR token, right?  :)
```

- Use the GitLab API to create a new repository

  ```bash
  curl -X POST -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" "https://gitlab.com/api/v4/projects?name=MyWebserver" | tee results
  ```

  You installed [jq](https://stedolan.github.io/jq/), right?  It's a handy tool for parsing JSON.  Piping that `curl` output to `tee` displays the results in the terminal as well as storing them in the `results` file.  With `jq`, the results are much more readable:

  ```bash
  cat results | jq
  # or
  cat results | jq '.ssh_url_to_repo'
  ```

- Clone the repository locally:

  ```bash
  cd ~/sfs
  git clone $(jq -r '.ssh_url_to_repo' results)
  cd mywebserver
  ```

  You should see this:

  ```
  osadmin@SFS-Eoan:~/sfs$ git clone $(jq -r '.ssh_url_to_repo' results)
  Cloning into 'mywebserver'...
  warning: You appear to have cloned an empty repository.
  osadmin@SFS-Eoan:~/sfs$ cd mywebserver
  osadmin@SFS-Eoan:~/sfs/mywebserver$
  ```

---

#### Why API?

Using Application Programming Interfaces (API's) allow us to control services without clicking around a user interface.  In this case, we're not passing any arguments when we create the repo.  But we could.  If you want to standardize all the repositories for your organization, using the API will ensure a faster, more consistent process.

![image](create_repo_gitlab.png?)

![image](git_clone_repo_gitlab.png?)

---

### Working With Your Repo

Reminder: You should be in `~/sfs/mywebserver`

```bash
cd ~/sfs/mywebserver
```

- Create a new file

  ```bash
  echo "hello world" > gitlablab
  ```

- Check the status

  ```bash
  git status
  ```

- Track the file

  ```bash
  git add gitlablab
  ```

- Make a commit

  ```bash
  git commit -m 'add hello world'
  ```

- Make a change

  ```bash
  echo "this is a mistake" > gitlablab
  git add gitlablab
  git commit -m 'fix the file'
  ```

- You have everything locally, but you want to push it to GitLab so...
  - You have a backup
  - You can collaborate with others  

- Store all commits on GitLab

  ```bash
  git push
  ```

  Navigate to your project and check out your file:

  ```bash
  jq -r '.web_url' ~/sfs/results
  ```

  ^ Open that URL in a browser

- We messed up our file!  Let's fix it.

  ```bash
  echo "Hello World" > gitlablab
  git add gitlablab
  git commit -m 'actually fixed this time'
  git push
  ```

- Just for fun, let's delete (make sure your `git push` worked!) and recover our local copy of the file...

  ```bash
  ls -l
  rm gitlablab
  ls -l
  git checkout gitlablab
  ls -l
  ```


- **[extra credit]** git diff command - enter the below commands and then git checkout prev version from last commit

  ```bash
  echo "replacement" > gitlablab
  echo "enhancement" >> gitlablab
  git diff
  ```
---

### Clone The Class Materials

```bash
git clone https://gitlab.com/sofreeus/introtodevops.git ~/sfs/introtodevops
```

---

### Create Your Own Project for This Class

```bash
cd ~/sfs
mkdir work-dir
cd work-dir
git init
```

The `git init` line tells git to start tracking this folder as a repository.  It doesn't matter that there's no remote GitLab repository.  You can still manage branches and commits locally.


If you're a really good instruction-follower, your `~/sfs` directory tree should look something like this:

```
$ tree -F -L 2 sfs
sfs
├── introtodevops/
│   ├── Cleanup.md
│   ├── labs/
│   ├── LICENSE
│   └── README.md
├── mywebserver/
│   └── gitlablab
├── results
└── work-dir/
```

---

### Further reading
  - [Gitlab API Documentation](https://docs.gitlab.com/ee/api/)
  - [Basic git terminology](http://juristr.com/blog/2013/04/git-explained/#Terminology)
  - [More advanced terminology](https://cocoadiary.wordpress.com/2016/08/17/git-terminologyglossary/)
  - [Revert a commit](https://git-scm.com/docs/git-revert.html) (a.k.a. Undo; restoring from backup)
  - [Branching and merging](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
  - [Git best practices](http://kentnguyen.com/development/visualized-git-practices-for-team/) (There are a lot of these.  Read critically and pick what works best in your environment.)
  - [Stashing](https://git-scm.com/book/en/v1/Git-Tools-Stashing)

---

| Previous: [Bash Warmup](/labs/00_bash_warmup) | Next: [Docker](/labs/02_docker) |
|---:|:---|
