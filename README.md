# Intro to DevOps

---

### Class Goal

DevOps is an exceptionally broad discipline.  It requires development expertise, operations sensibility, an immense capacity for troubleshooting, eagerness to learn, and - maybe most importantly - a broad base of knowledge around tools that can be stitched together to accomplish the goal.  

This class will provide you with a very basic framework for workflow automation.  Every commit to a repo will be built, tested, and deployed.

There is no Holy Grail - every environment is different.  This course attempts to use mostly free, open-source software to show that workflow automation is a real thing that can be done.  We'll cover one way to do it, but there are many, many other ways to accomplish the same thing.  (I've also done a GitHub --> CircleCI --> AWS/Mesos/Marathon setup that does something very similar to what we'll see in this course.)

---

### Success Metric

Measure - or estimate - the time it takes for a code commit to be deployed in your production environment.  This is the main metric we'll use to measure the success of the DevOps workflow we build in this class.

---

### Prerequisites:

- Before starting this class, you should have a basic familiarity with a Unix-like shell (sh, bash, zsh, ksh, etc.)
- Laptop
  - Something with a Bash-like shell is preferred (Mac or Linux)
  - Windows machines may be a challenge (Maybe use [VirtualBox](https://www.virtualbox.org/) to run a Linux VM?)
  - Make sure you have [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed
  - Software Freedom School is not responsible for data loss on your laptop.  We try to keep things in an `~/sfs` directory tree, but some tool installation and configuration may conflict with something you already have on your machine.  Running this class in a VM can help keep your other stuff safer.
- [Create a GitLab account](https://gitlab.com/users/sign_in)
  - If you already have one, you can disregard this
  - If you create a new one, you might consider making it something that you could take from company to company
- [Install Docker on your laptop](https://hub.docker.com/search/?type=edition&offering=community)
- [Create a GCP account](https://cloud.google.com/)
- Install [jq](https://stedolan.github.io/jq/)
- Code editor:
  - We recommend [Atom](https://atom.io/) (instructions below) with the following packages installed:
    - language-yaml (should be included by default as a core package)
    - language-docker
  - VI/VIM or VS Code are also popular and acceptable options

***

### Following Instructions

Please follow the documented and stated instructions as closely as possible.  This will help mitigate issues that arise due to funky configurations.  As mentioned above, we'll be stitching together a number of tools.  This means that the labs are very inter-dependent, and an innocent deviation in an early lab could complicate a later lab.

I encourage all students to experiment and explore the material.  Making it your own and having fun with it will probably increase the functional utility of this class immensely.  I'm happy to help with any extracurricular questions and/or interests related to the material, but please try the extra stuff after completing the suggested stuff.  And maybe in a different subdirectory.  :)

***

# Labs

### 00. [Bash Warmup](/labs/00_bash_warmup)

### 01. [GitLab](/labs/01_gitlab)

### 02. [Docker](/labs/02_docker)

### 03. [Webserver](/labs/03_webserver)

### 04. [Continuous Integration](/labs/04_continuous_integration)

### 05. [GCP & GKE](/labs/05_gcp_and_gke)

### 06. [Manual Deployment](/labs/06_manual_deployment)

### 07. [Authenticating Kubernetes to GitLab](/labs/07_auth_kubernetes_to_gitlab)

### 08. [Authenticating GitLab to Kubernetes](/labs/08_auth_gitlab_to_kubernetes)

### 09. [Automated Deployment](/labs/09_automated_deployment)

### 10. [Everything](/labs/10_everything) (a.k.a. The Easy Lab)

***

### Bonus Labs

### + [Job Templates & Environments](/labs/11_one_more_step)

### + [Live Testing & Variable Abstraction](/labs/12_yet_another_step)

### + [Configuration Management](/labs/bonus/configuration_management.md)

### + [Team Organization](/labs/bonus/team_organization.md)

---

### Recommended Reading
- [The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509/), by Gene Kim
  - This is a rewrite of *The Goal* for the modern age.
- [The Goal](https://www.amazon.com/Goal-Process-Ongoing-Improvement/dp/0884271951/), by Eliyahu Goldratt
  - This is the original, and I think you'll benefit from doing the modern adaptation yourself.

---

### Installing Atom on the SFS VM

```bash
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list'
sudo apt update
sudo apt install -y atom
```

---

### Installing Docker on the SFS VM

```bash
sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu disco stable"
sudo apt update
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker ${USER}
sudo reboot
```
